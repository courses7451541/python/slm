import json
from numpy.random import choice


def get_array_from_txt():
    words = open('Data/data.txt', 'r')
    tab_word = []
    for word in words:
        clean_word = word.strip().lower()
        clean_word += '#'
        tab_word.append(clean_word)
    return tab_word


def prepare_dico(dico_data, tab_words, index_accurate):
    for word in tab_words:
        for index_letter in range(0, len(word)):
            set_up_dico(dico_data, word, index_letter + 1 - index_accurate, 1, index_accurate)
            index_letter += 1
    save_data(dico_data)  # uncomment to save the data


def set_up_dico(dico, word, index_word, index_accurate_current, index_accurate):
    if index_accurate_current == index_accurate:
        if index_word >= 0:
            if word[index_word] not in dico:
                dico[word[index_word]] = 1
            else:
                dico[word[index_word]] += 1
        else:
            if '-' not in dico:
                dico['-'] = 1
            else:
                dico['-'] += 1
    elif index_word < 0:
        if '-' not in dico:
            dico['-'] = {}
        return set_up_dico(dico['-'], word, index_word + 1, index_accurate_current + 1, index_accurate)
    else:
        if word[index_word] not in dico:
            dico[word[index_word]] = {}
        return set_up_dico(dico[word[index_word]], word, index_word + 1, index_accurate_current + 1, index_accurate)


def analyse_data(dico, index_accurate):
    if index_accurate >= 1:
        for element in dico.values():
            if type(element) is dict:
                if analyse_data(element, index_accurate - 1) == -1:
                    nb_total_occurrence = sum(element.values())
                    for key in element:
                        element[key] = element[key] / nb_total_occurrence
            else:
                return -1


def save_data(dico_data):
    with open('Data/data_clean.json', 'w', encoding='utf8') as f:
        json.dump(dico_data, f, ensure_ascii=False)


def get_letter(dico, word, index_accurate):
    if index_accurate > 1:
        return get_letter(dico[word[len(word) - index_accurate + 1]], word, index_accurate - 1)
    else:
        return choice([*dico.keys()], p=[*dico.values()])


def generate_word(dico, index_accurate):
    word = ""
    for i in range(1, index_accurate):
        word += "-"
    while word[-1] != '#':
        word += get_letter(dico, word, index_accurate)
    word = word.replace('-', "")
    word = word.replace('#', "")
    return word


def generate_words(dico, index_accurate, num_words=5):
    generated_words = []
    for _ in range(num_words):
        word = generate_word(dico, index_accurate)
        generated_words.append(word)
    return generated_words


def get_data():
    with open('Data/data_clean.json', 'w') as file:
        json.dump({}, file, ensure_ascii=False)
    with open('Data/data_clean.json', 'r') as file:
        return json.load(file)


if __name__ == '__main__':
    array_words = get_array_from_txt()
    # array_words = ['toto','titi','bonjour','mesure']
    dico_data = get_data()
    prepare_dico(dico_data, array_words, 3)
    with open('Data/data_trained.json', 'w',
              encoding='utf8') as trained:  # uncomment to get the data trained in new file
        analyse_data(dico_data, 3)
        json.dump(dico_data, trained, ensure_ascii=False)

    with open('Data/data_trained.json', 'r') as dico:
        dico_data = json.load(dico)
    list_word = generate_words(dico_data, 3)
    print(list_word)
