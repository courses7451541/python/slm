# SLM project

![release](https://img.shields.io/badge/release-2.0.0-blue)

---

## Summary

* [Consignes](#consignes)
* [Credit](#credit)

---

## Consignes

Création d'un générateur de mot aléatoire à consonance française, sur la base d'une liste de mots français

1. Obtention d'une liste de mots français (et traitement, si nécessaire)
2. Établissement d'un tableau de probabilité de N dimensions pour chaque lettre (On considère "début de mot" et "fin de mot" comme des caractères)
3. Utilisation de ce tableau pour générer des mots à consonance française 

La génération de mot doit évidemment être séparée de l'établissement du tableau. On ne veut pas le ré-établir à chaque lancement du générateur. Donc, le tableau doit être enregistré. 

---

## Credit

![Paul Hoareau](https://img.shields.io/badge/Paul-Hoareau-red?style=for-the-badge&logo=superuser)
![Tony Le Fur Joguet](https://img.shields.io/badge/Tony-Le%20Fur%20Joguet-red?style=for-the-badge&logo=superuser)
![Pierre Saugues](https://img.shields.io/badge/Pierre-Saugues-red?style=for-the-badge&logo=superuser)
